#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void readline(char input[])
{
	int c;
	int i;

	i = 0;

	while ((c = getchar()) != '\n')
	{
		input[i] = c;
		i++;
	}
	input[i] = '\00';
}

void print_field(int field[3][3])
{
	for (int i = 0; i < 100; i++)
		printf("\n");

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (field[i][j] == 0)
				printf("X");
			else if (field[i][j] == 1)
				printf("O");
			else
				printf("-");
			printf(" ");
		}
		printf("\n");
	}
}

int figure_out_winner(int field[3][3])
{
	int winner;

	/*
	X o x
	X o x
	X o o
	*/
	for (int i = 0; i < 3; i++)
	{
		winner = field[i][0];
		for (int j = 0; j < 3; j++)
		{
			if (field[i][j] != winner)
			{
				winner = -1;
				continue;
			}
		}

		if (winner == field[i][0])
			return winner;
	}

	/*
	X X X
	o o x
	o x o
	*/
	for (int j = 0; j < 3; j++)
	{
		winner = field[0][j];
		for (int i = 0; i < 3; i++)
		{
			if (field[i][j] != winner)
			{
				winner = -1;
				continue;
			}
		}

		if (winner == field[0][j])
			return winner;
	}

	// kein diagonal weil keine lust lg

	return -1;
}

void program(int endless)
{
	int field[3][3];
	char input[256];
	char input_char;
	int current[2];
	int current_player;
	int winner;
	int bot_v_bot;
	unsigned int seed;

	if (endless == 0)
	{
		printf("--- AI TicTacToe ---\n");
		usleep(1000000);
		printf("AI mehr wie zufällig lol\n");
		usleep(500000);
	}

	
	current_player = 0;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			field[i][j] = -1;
		}
	}

	if (endless == 0)
	{
		printf("Was tun?\n1: Neues zufälliges Match\n2: Match wieder abspielen/eigener Seed\n");
		readline(input);
	}
	else
	{
		input[0] = '1';
		input[1] = '\00';
	}

	if (strcmp(input, "1") == 0)
	{
		seed = time(NULL);
		srand(seed);
		
		seed = rand();
		srand(seed);
	}
	else
	{
		printf("Seed (nur Zahlen, >0; <4294967295): ");
		readline(input);
		seed = atoi(input);
		srand(seed);
	}

	if (endless == 0)
	{
		printf("Bot gegen Bot (1) oder Spieler gegen Bot (2)?\n");
		readline(input);
		if (strcmp(input, "1") == 0)
			bot_v_bot = 1;
		else
		{
			bot_v_bot = 0;
			print_field(field);
		}
	}
	else
	{
		bot_v_bot = 1;
	}
	

	for (int i = 0; i < 9; i++)
	{
		if (current_player == 0 && bot_v_bot == 0)
		{
			do
			{
				printf("Wo Feld platzieren (Format: x,y)?\n");
				readline(input);

				current[1] = atoi(&input[0]) - 1;
				current[0] = atoi(&input[2]) - 1;
			} while (field[current[0]][current[1]] > -1);
		}
		else
		{
			do
			{
				current[0] = rand() % 3;
				current[1] = rand() % 3;
			} while (field[current[0]][current[1]] > -1);
			
		}
		
		field[current[0]][current[1]] = current_player;

		current_player++;
		current_player = current_player % 2;

		print_field(field);
		winner = figure_out_winner(field);
		if (winner > -1)
		{
			break;
		}

		if (endless == 0)
		{
			printf("Enter für den nächsten Zug");
			readline(input);
		}
		else
			usleep(100000);
	}

	if (winner > -1)
	{
		if (winner == 0)
			printf("X gewinnt!\n");
		else if (winner == 1)
			printf("O gewinnt!\n");
	}
	else
		printf("Niemand gewinnt!\n");

	printf("Seed: %u\n", seed);
	if (endless == 1)
		usleep(1000000);
		printf("\n\n");

}

int main()
{
	char input[256];
	printf("Endless mode aktivieren? (1 = ja)\n");
	readline(input);

	if (strcmp(input, "1") == 0)
	{
		while (1)
			program(1);
	}
	else
		program(0);

	return 0;
}
